#!/bin/sh
# -*- mode: scheme -*-
# Store where we are run from. Is (among possibly other things) used for getting
# the git version for development builds.
export __SSG_PATH="$(dirname "$(realpath "$0")")"
GUILE=${GUILE:-guile}
exec "$GUILE" -e main -s "$0" "$@"
!#

(add-to-load-path (string-append (dirname (current-filename)) "/module"))
(add-to-load-path "/home/hugo/code/calp/module")

(use-modules (srfi srfi-1)
             (hnh util path)
             (ice-9 getopt-long)
             (ice-9 ftw)
             (mkdir)
             (ssg)
             ((ssg assets) :select (add-to-asset-path))
             )

(set! *random-state* (random-state-from-platform))

(define (print-help)
  (format #t "Usage: ssg publish [-o dir] <path-to-page>~%"))

(define command-line-options
  `((help (single-char #\h))
    #; (output (single-char #\o) (value #t))
    ))

(define publish-options
  `((output (single-char #\o) (value #t))
    (assets (value #t))
    ))



(define (publish args)
  (define options (getopt-long args publish-options
                               #:stop-at-first-non-option #t))
  ;; input directory
  (define page-dir (string-join (option-ref options '() '())))

;;; Add asset paths.
;;; Will result in a list at most containing the following (in order of priority)
;;; - ${user_supplied_asset_path}
;;; - ${SITE_DIR}/assets
;;; - ${__SSG_PATH}/assets
;;; - ${XDG_DATA_HOME}/ssg/assets
;;; - ${d}/ssg/assets for d in ${XDG_DATA_DIRS}

  ;; Check where we are running from. If it's a /bin/ directory then we are most
  ;; likely installed, and shouldn' check for relative assets. However, if we
  ;; aren't in a bin directory we are probably in our own source directoroy,
  ;; where an /assets/ directory exists.
  (cond ((getenv "__SSG_PATH")
         => (lambda (ssg-path)
              (unless (string=? "bin" (basename ssg-path))
                (add-to-asset-path (path-append ssg-path "assets"))))))

  ;; A page can supply their own asset overrides
  (add-to-asset-path (path-append page-dir "assets"))

  ;; User overrides
  (cond ((option-ref options 'assets #f)
         => add-to-asset-path))

  ;; Run actual generator
  (let ((output-dir (realpath (option-ref options 'output "output"))))
    (mkdir-p output-dir)
    (generate-site page-dir output-dir)))



(define (main args)
  (define options (getopt-long args command-line-options
                               #:stop-at-first-non-option #t))

  (define tail-commands (option-ref options '() '()))

  (when (or (option-ref options 'help #f)
            (null? tail-commands))
    (print-help)
    (if (null? tail-commands)
        (exit 1)
        (exit 0)))

  (case (string->symbol (car tail-commands))
    ((widgets)
     (display-widgets %load-path)
     (exit 0))
    ((content-type)
     (display-content-types %load-path)
     (exit 0))
    ((publish) (publish tail-commands))
    ;; NOTE this case works if no arguments are to be passed to publish. However,
    ;; `ssg -o output my-site` won't work since -o isn't a top level option,
    ;; which causes getopt-long to directly call (exit 1).
    (else (publish (cons "publish" tail-commands)))))
