(define-module (ssg current-page)
  :export (current-page))

;; Current destination page
(define current-page (make-parameter "??NO_CURRENT_PAGE??"))
