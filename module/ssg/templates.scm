(define-module (ssg templates)
  :use-module ((sxml transform) :select (pre-post-order))
  :use-module (hnh util path)
  :use-module ((ssg lib) :select (read-entry))
  :use-module ((ssg content-type) :select (parser-for))
  :use-module ((ssg widgets) :select (apply-widget))
  :export (expand-templates
           read-template)
   )

;; expands all *WIDGET* instances, recursively
(define (expand-templates sxml)
  (pre-post-order sxml
               `((*WIDGET* *macro* . ,(lambda (_ data) (apply-widget data)))
                 (*default* . ,list)
                 (*text* . ,(lambda (_ text) text)))))


(define (read-template template)
  (call-with-values (lambda () (read-entry (path-append "templates" template)))
    (case-lambda ((header body)
                  ((parser-for
                    (or (assoc-ref header 'content-type)
                        "text/html"))
                   body))
                 ((body)
                  ((@ (ssg content-type text html) body->html)
                   body)))))
