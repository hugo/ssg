(define-module (ssg entries)
  :use-module (rnrs records syntactic)
  :use-module (srfi srfi-1)
  :use-module (ice-9 ftw)
  :use-module (hnh util path)
  :use-module (ssg file-set)
  :use-module ((hnh util) :select (sort* label))
  :use-module (datetime)
  :use-module (sxml simple)
  :use-module ((ssg lib) :select (read-entry url-append))
  :use-module ((ssg content-type) :select (parser-for))
  :use-module (ssg templates)
  :use-module (ssg widgets)
  :use-module (ssg config)
  :use-module ((ssg widgets page-body) :select (page-body))
  :use-module ((ssg widgets page-title) :select (page-title))
  :use-module ((ssg widgets entry-date) :select (entry-date))
  :use-module ((ssg widgets entry-permalink) :select (entry-permalink))
  :export (entry-body entry-template entry-header
                      entries
                      load-entries!
                      entries-in-reverse-chronological-order
                      entry-path entry-slug
                      slug-chars
                      ))

(define-record-type entry
  (fields headers body page-template body-template))

(define (entry-header entry key)
  (assoc-ref (entry-headers entry) key))

(define (regular-file? filename)
  (eq? 'regular (stat:type (stat filename))))


(define (entry-files)
  (filter regular-file?
          (map (lambda (file) (path-append "entries" file))
               (remove file-hidden?
                       (scandir "entries")))))

(define entries '())

(define (load-entries! fs config)
  (set! entries
    (map (lambda (in-path)
           (let ((entry (load-entry in-path config)))
             (file-set-add! fs (entry-path entry)
                            (delay (render-entry entry)))
             entry))
         (entry-files))))



;; Loads an entry from disk into an entry object
(define (load-entry path config)
  (call-with-values (lambda () (read-entry path))
    (label loop
           (case-lambda
             ((headers body)
              (let ((page-template-name
                     (or (assoc-ref headers 'template)
                         (assoc-ref config 'default-template)
                         (error "Entry page template Required")))
                    (body-template-name
                     (or (assoc-ref headers 'body-template)
                         (assoc-ref config 'entry-body-template)
                         (error "Entry body template required"))))
                (make-entry headers body
                            page-template-name
                            body-template-name)))
             ((body)
              (display (format #f "Entry ~a lacks headers~%" path) (current-error-port))
              (loop '() body))))))



;; Rendere a single entry
(define (render-entry entry)
  (let ((page-template-name (entry-page-template entry))
        (body-template-name (entry-body-template entry)))
    (let ((body-template-ast (read-template body-template-name))
          (page-template-ast (read-template page-template-name))
          (body-ast ((parser-for (or (entry-header entry 'content-type)
                                     "text/html"))
                     (entry-body entry))))
      (lambda (destination)
        (define page-template-resolved (expand-templates page-template-ast))
        (define body-template-resolved (expand-templates body-template-ast))
        (define body-resolved (expand-templates body-ast))
        (define final-tree
          (parameterize ((page-title (entry-header entry 'title))
                         (entry-date (entry-header entry 'date))
                         (entry-permalink (entry-slug entry)))
            (parameterize ((page-body
                            (parameterize ((page-body
                                            (force-widgets body-resolved)))
                              (force-widgets body-template-resolved))))
              (force-widgets page-template-resolved)
              )))
        (with-output-to-file (path-append destination (entry-path entry))
          (lambda () (sxml->xml final-tree)))))))

;;; ----------------------------------------

(define (cs-range start end)
  "Generate a character set for the characters between start and end, both inclusive."
  (ucs-range->char-set (char->integer start)
                       (1+ (char->integer end))))


;; URL safe characters (see RFC 3986)
(define slug-chars (make-parameter
                    (char-set-union (cs-range #\0 #\9)
                                    (cs-range #\a #\z)
                                    (char-set #\. #\~ #\_ #\-))))


(define (slugify str)
  "Transform @var{str} to something safe for a URL."
  (string-map
   (lambda (c)
     (if (char-set-contains? (slug-chars) c)
         c #\-))
   ;; A transliterate step here would also be good
   (string-downcase str)))


(define (entry-path/slug entry joiner)
 (define entry-dir (or (assoc-ref (config) 'entry-dir)
                        "entries"))
 (cond ((entry-header entry 'slug)
        ;; NOTE should slug be validated?
        => (lambda (slug) (joiner entry-dir (string-append slug ".html"))))
       ((entry-header entry 'date)
        => (lambda (date) (joiner entry-dir
                             (format #f "~a-~a.html"
                                     date (slugify (entry-header entry 'title))))))
       (else (joiner entry-dir
                     (format #f "~a.html"
                             (slugify (entry-header entry 'title)))))))


;; entry-path and entry-slug are really similar. Difference being that
;; entry-path gives a (relative) local file system path, while entry
;; slug gives a web path.

(define (entry-path entry)
  (entry-path/slug entry path-append))

(define (entry-slug entry)
  (entry-path/slug entry url-append))

;;; ----------------------------------------

(define (entries-in-reverse-chronological-order)
  (sort* entries datetime>
         (lambda (e) (as-datetime (string->date/-time (entry-header e 'date))))))
