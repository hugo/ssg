(define-module (ssg lib)
  :use-module (rnrs io ports)
  :use-module ((srfi srfi-1) :select (every fold reduce))
  :use-module (ice-9 regex)
  :use-module (ice-9 format)
  :use-module (ice-9 curried-definitions)
  :use-module ((hnh util) :select (begin1))
  :export (read-key-value
           read-entry
           link-or-copy
           symlink-if-not-exists
           accumulate
           flip
           squeeze-repeats
           url-join
           url-append
           ))


(define (accumulate proc default lst)
  (if (null? lst)
      default
      (fold (lambda (item done)
              (cons (proc item (car done))
                    done))
            (list (car lst))
            (cdr lst))))


(define ((flip f) . args)
  (apply f (reverse args)))


(define (read-key-value port)
  (let loop ((kvs '()) (line-number 1))
    (let ((line (get-line port)))
      (cond ((or (eof-object? line) (string-null? line))
             kvs)
            ;; Comments
            ((string-match "^ *#" line)
             => (lambda _ (loop kvs (1+ line-number))))
            ;; Directives
            ((string-match "^ *([^ :]+): *(.*) *$" line)
             => (lambda (m)
                  (loop (acons
                         (string->symbol (match:substring m 1))
                         (match:substring m 2)
                         kvs)
                        (1+ line-number))))
            (else (format (current-error-port)
                          "Error processing ~a, line ~a, line contains invalid attribute: ~s~%"
                          (port-filename port)
                          line-number
                          line)
                  ;; return what we have
                  kvs)))))

(define (first-line-contains-colon? port)
  "Does the first line of the input stream contain a colon?"
  (let ((line (get-line port)))
    (if (eof-object? line)
        line
        (begin1
         (string-index line #\:)
         (unread-string "\n" port)
         (unread-string line port)))))

;; Reads a blog entry from filename
;; returns a pair of key-value pairs, and the body
(define (read-entry filename)
  (call-with-input-file filename
    (lambda (port)
      (cond ((first-line-contains-colon? port)
              (values
               (read-key-value port)
               (get-string-all port)))
            (else (get-string-all port))))))



(define (link-or-copy from to)
  "Try hardlinking @var{from} to @var{to}, falling back to copying if link failed."
  (catch 'system-error
    (lambda () (link from to))
    (lambda (err proc fmt args data)
      (cond ((= EXDEV (car data)) (copy-file from to))
            ((= EEXIST (car data))
             ;; Indiscriminating removing and re-linking works in
             ;; theory, but sometimes fails over NFS.
             (unless (= (stat:ino (stat to))
                        (stat:ino (stat from)))
               (delete-file to)
               (link-or-copy from to)))
            (else (scm-error err proc fmt args data))))))


(define (symlink-if-not-exists from to)
  (catch 'system-error
    (lambda ()
      (let ((current (readlink to)))
        (unless (string=? from current)
          (scm-error 'misc-error "symlink-if-not-exists"
                     "Failed to create symlink ~s -> ~s, already points at ~s"
                     (list from to current)
                     #f))))
    (lambda (err proc fmt args data)
      (if (= ENOENT (car data))
          (symlink from to)
          (scm-error err proc fmt args data)))))


(define (squeeze-repeats str pattern)
  (regexp-substitute/global
   #f
   (format #f "(~a)+" pattern)
   str
   'pre pattern 'post))



;; Conceptually equivalent to path-join, but always uses forward slashes
(define (url-join components)
  (unless (every string? components)
    (scm-error 'wrong-type-arg "url-join"
               "All components of a url must be strings. Got: ~a"
               (list components) #f))
  (reduce (lambda (current done)
            (cond ((or (string-null? done)
                       (string-null? current))
                   current)

                  ;; Both have a slash
                  ((and (char=? #\/ (string-ref done (1- (string-length done))))
                        (char=? #\/ (string-ref current 0)))
                   (string-append done (string-drop current 1)))

                  ;; Neither have a slash
                  ((and (not (char=? #\/ (string-ref done (1- (string-length done)))))
                        (not (char=? #\/ (string-ref current 0))))
                   (string-append done (string-append "/" current)))

                  ;; One of them have a slash
                  (else
                   (string-append done current))))
          ""
          components))

(define (url-append . components)
  (url-join components))
