(define-module (ssg content-type)
  :export (parser-for)
  )

;; something like "text/html" -> <a procedure>
(define (parser-for content-type)
  (define interface (resolve-interface
                     (if (not content-type)
                         '(ssg content-type text plain)
                         `(ssg content-type ,@(map string->symbol
                                                   (string-split content-type #\/))))))
  (module-ref interface 'body->html))
