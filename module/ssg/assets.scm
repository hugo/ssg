(define-module (ssg assets)
  :use-module (hnh util path)
  :use-module ((xdg basedir) :prefix xdg-)
  :use-module (srfi srfi-41)
  :use-module (srfi srfi-41 util)
  :use-module ((srfi srfi-71) :select (uncons))
  :export (add-to-asset-path asset-paths get-asset))

;; Additional (user configurable) asset paths
(define asset-paths (make-parameter '()))

(define (add-to-asset-path path)
  (asset-paths (cons path (asset-paths))))

(define (get-asset target)
  "Find asset in asset paths."
  (uncons
   (stream-first-value
    (stream-map
     (compose open-or-false
              (lambda (path) (path-append path target)))
     (stream-append (list->stream (asset-paths))
                    (stream-map (lambda (path) (path-append path "ssg" "assets"))
                                (stream-cons (xdg-data-home) (list->stream (xdg-data-dirs))))
                    ;; This raises an exception if we ever try to look at the end of stream.
                    (stream (scm-error 'system-error "get-asset"
                                       "No asset file ~a" (list target) (list ENOENT))))))))


;;; Internal details


;; Returns file path an open input stream if file exists, or #f if it doesn't.
;; Note that it also succeeds for non-regular files.
(define (open-or-false path)
  (catch 'system-error
    (lambda () (cons path (open-input-file path)))
    (lambda (err proc fmt args data)
      (if (= ENOENT (car data))
          #f
          (scm-error err proc fmt args data)))))


;; Get first non-false value in stream. Crashes in an undefined maner if none exists.
(define (stream-first-value strm)
  (stream-car (stream-drop-while not strm)))
