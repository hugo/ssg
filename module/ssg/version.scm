(define-module (ssg version)
  :use-module (srfi srfi-71)
  :use-module (ice-9 popen)
  :use-module (ice-9 rdelim)
  :use-module ((hnh util env) :select (with-working-directory))
  :export (version))

;; Get current version from git if available, and fall back to a hard coded
;; default (which should be updated on each release).
(define version
  (let ((line status
              (call-with-output-file "/dev/null"
                (lambda (dev-null)
                  (define pipe
                    (with-error-to-port dev-null
                      (lambda ()
                        (with-working-directory
                         ;; This __SSG_PATH should be set by the program entry point
                         (or (getenv "__SSG_PATH") ".")
                         (lambda ()
                           (open-input-pipe "git describe --tags --match 'v[0-9].[0-9]*'"))))))
                  (define line (read-line pipe))
                  (values line (close-pipe pipe))))))
    (if (zero? (status:exit-val status))
        (string-drop line 1)
        ;; version marker for a build tool to replace.
        #|@version@|# "0.1")))
