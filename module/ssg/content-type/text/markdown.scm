(define-module (ssg content-type text markdown)
  :use-module (commonmark)
  :use-module (sxml transform)
  :use-module (ice-9 regex)
  :export (body->html))

;; (commonmark->sxml "`a`")
;; ⇒ ((p (code "a")))
;; (commonmark->sxml "```bash\na\n```")
;; ⇒ ((pre (code (@ (class "language-bash")) "a")))

(define (handle-code _ tree)
  (cond ((not (string? tree)) tree)
        ((string-match "^!!! *(.*)" tree)
         => (lambda (m) `(*WIDGET* ,(match:substring m 1))))
        (else tree)))

(define (body->html string)
  (define tree (cons '*TOP* (commonmark->sxml string)))
  (pre-post-order tree
   `((code . ,handle-code)
     (pre ((code . ,list)) . ,list)
     (*default* . ,list)
     (*text* . ,(lambda (_ text) text)))))
