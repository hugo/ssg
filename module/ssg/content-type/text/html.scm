(define-module (ssg content-type text html)
  :use-module (ice-9 regex)
  :export (body->html)
  )

;; Something on the form `[[widget-name parameters]]'
(define widget-rx (make-regexp "[[]{2}([^]]*)[]]{2}"))

;; Takes an input string, and returns an SXML document
;; Lambdas in body is to force direct output of input, since we want te preserve
;; the output the user gave us.
(define (body->html string)
  (cons '*TOP*
        (let loop ((remaining string))
          (cond ((regexp-exec widget-rx remaining)
                 => (lambda (m)
                      (cons*
                       (lambda () (display (substring/shared remaining 0 (match:start m))))
                       `(*WIDGET* ,(match:substring m 1))
                       (loop (substring/shared remaining (match:end m))))
                      ))
                (else (list (lambda () (display remaining))))))))
