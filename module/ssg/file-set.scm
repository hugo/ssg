(define-module (ssg file-set)
  :use-module (rnrs records syntactic)
  :use-module (ice-9 format)
  :use-module (hnh util path)
  :use-module (mkdir)
  :export (make-file-set
           file-set?
           file-set-add!
           file-set-keys
           realize-file-set
           (file-set-parameter . file-set)))

(define-record-type (file-set make-file-set% file-set?)
  (fields map))

(define (make-file-set)
  (make-file-set% (make-hash-table)))

(define file-set-parameter (make-parameter (make-file-set)))

(define entry-cookies (make-hash-table))

(define* (file-set-add! file-set path* file-producer
                        #:key
                        (cookie (random (expt 2 64)))
                        (on-collision
                         (lambda (path cookie*)
                           (unless (equal? cookie cookie*)
                             ;; TODO could we get offending page here?
                             (display (format #f "File already existing at ~s, ~s ~s overwriting~%"
                                              path cookie cookie*)
                                      (current-error-port))))))
  (let ((path (string-trim path* #\/)))
    (when (hash-ref (file-set-map file-set) path)
      (on-collision path (hash-ref entry-cookies path)))
    (hash-set! entry-cookies path cookie)
    (hash-set! (file-set-map file-set) path
               (force file-producer))

    file-set))

(define (file-set-keys fs)
  (hash-map->list (lambda (a _) a) (file-set-map fs)))

(define (realize-file-set fs destination)
  (hash-for-each
   (lambda (path proc)
     ;; TODO make error catching configurable
     (catch #t (lambda ()
                 (mkdir-p (dirname (path-append destination path)))
                 (parameterize (((@ (ssg current-page) current-page) path))
                   (proc destination)))
       (case-lambda
         ;; Well formed errors
         ((err proc fmt args data)
          ;; TODO Should this really be limited to a warning? Many of these
          ;;      causes the page to not render at all!
          (display (if proc
                       (format #f "WARNING (realize-file-set) in ~a: ~? for file ~s~%"
                               proc fmt args path)
                       (begin
                        (format #f "WARNING (realize-file-set): ~? (~a) for file ~s~%"
                                fmt
                                args
                                (if (and (not (null? args))
                                         (procedure? (car args))
                                         (procedure-documentation (car args)))
                                    (car
                                     (string-split (procedure-documentation (car args))
                                                   #\newline))
                                    "unknown widget")
                                path)))
                   (current-error-port)))
         ;; Other weird errors
         ((err . other)
          (display (format #f "~a: ~s~%" err other)
                   (current-error-port))))))
   (file-set-map fs)))

