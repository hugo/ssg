(define-module (ssg widgets)
  :use-module (srfi srfi-1)
  :use-module ((hnh util) :select (->))
  :use-module (ice-9 regex)
  :export (parse-widget
           apply-widget
           force-widgets
           )
  )

(define (resolve-widget widget)
  (define interface (resolve-interface `(ssg widgets ,widget)))
  (module-ref interface 'widget))


;; Tokenizes the body of a widget into something ready for a procedure call
(define (parse-widget str)
  (define tokens (string-tokenize str))
  (when (null? tokens) (error "Empty widget"))
  (let* ((widget-name (string->symbol (car tokens))))
    (cons
     widget-name
     (concatenate
      (map (lambda (s)
             (cond ((string-match "^[0-9]*$" s)
                    => (lambda (m) (list (string->number s))))
                   ((string-match "^([A-Za-z_-]+)=([0-9]+)$" s)
                    => (lambda (m) (list (-> (match:substring m 1)
                                        string->symbol symbol->keyword)
                                    (string->number (match:substring m 2)))))
                   ((string-match "^([A-Za-z_-]+)=(.+)$" s)
                    => (lambda (m) (list (-> (match:substring m 1)
                                        string->symbol symbol->keyword)
                                    (match:substring m 2))))
                   (else (list s))))
           (cdr tokens))))))


;; Takes a widget string, such as "[widget-name key=value]",
;; and call (@ (ssg widgets ,widget-name) widget) with key=value
;; as `#:key value' arguments
(define (apply-widget str)
  (define lst (parse-widget str))
  (catch 'misc-error
    (lambda () (apply (resolve-widget (car lst))
                 (cdr lst)))
    (lambda (err proc fmt args rest)
      (format (current-error-port)
              "~a when resolving widget: ~?~%"
              err fmt args)
      `(div (@ (class "broken-widget"))
            "[Broken widget " (code ,str) "]"))))


(define (force/all-in-tree tree)
  (cond ((null? tree)     '())
        ;; TODO Should the forced result also be expanded?
        ((promise? tree) (force tree))
        ((pair? tree)
         (cons (force/all-in-tree (car tree))
               (force/all-in-tree (cdr tree))))
        (else tree)))

(define (force-widgets tree)
  (force/all-in-tree tree))
