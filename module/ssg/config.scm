(define-module (ssg config)
  :use-module (ice-9 regex)
  :export (config current-scheme default-config))

(define default-config
  `((title . "You forgot to set a title")))

;; TODO generalized way to set default, and conditional defaults, for configuration items
(define (current-scheme)
  (or (assoc-ref (config) 'scheme)
      (cond ((string-match "^localhost($|:)" (assoc-ref (config) 'host))
             "http")
            (else "https"))))

(define config (make-parameter default-config))
