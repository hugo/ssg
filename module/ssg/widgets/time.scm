(define-module (ssg widgets time)
  :use-module (datetime)
  :export (doc widget))

(define doc "
")

(define date-format (make-parameter "~Y-~m-~d"))
(define time-format (make-parameter "~H:~M"))
(define datetime-format (make-parameter "~Y-~m-~d ~H:~M"))

(define (widget utc-time)
  "time

Given a date, time, or datetime object: emit the propper <time/> tag."
  (define dt (string->date/-time utc-time))
  (delay
    `(time (@ (datetime ,utc-time))
           ,(datetime->string
             (as-datetime dt)
             (cond ((date? dt) (date-format))
                   ((time? dt) (time-format))
                   (else (datetime-format)))))))
