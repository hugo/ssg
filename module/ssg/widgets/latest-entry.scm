;;; Commentary:
;;; Returns the latest entry in an article tag
;;; Code:

(define-module (ssg widgets latest-entry)
  :use-module (datetime)
  :use-module (hnh util)
  :use-module ((ssg content-type) :select (parser-for))
  :use-module (srfi srfi-1)
  :use-module (ssg entries)
  :export (doc widget))

(define doc "
Shows the most recent entry in an article tag.

Ignores all its arguments.
")

(define (widget . _)
  "latest-entry

Emits the latest published entry."
  (define entries (entries-in-reverse-chronological-order))
  (if (null? entries)
      (delay
       `(article (@ (class "inline-entry"))
                 (header (h3 "NO ENTRIES FOUND"))))
      (let* ((entry (car entries))
             (formatted
              `(article (@ (class "inline-entry"))
                        (header (h3 ,(entry-header entry 'title))
                                (span (@ (class "metainfo"))
                                      (dl (dt "Publicerad")
                                          (dd ,(entry-header entry 'date))
                                          (dt "Publicerad av")
                                          (dd ,(entry-header entry 'author)))))
                        ,((parser-for (entry-header entry 'content-type)) (entry-body entry)))))
        (lambda () formatted))))


;; TODO possibly rewrite this widget to use HTML as input, to demonstrate the capability

;; "
;; <article>
;;   <header>
;;     <h3>[[header-ref title]]</h3>
;;     <span class='metainfo'>
;;       <dl>
;;         <dt>Publicerad</dt><dd>[[header-ref date]]</dd>
;;         <dt>Publicerad av</dt><dd>[[header-ref author]]</dd>
;;       </dl>
;;     </span>
;;   </header>
;;   [[body]]
;; </article>
;; "

#|
<article>
  <header><h3>[[title]]</h3>
    <span class="metainfo">
      <dl><dt>Publicerad</dt   ><dd>[[header-ref date]]</dd>
          <dt>Publicerad av</dt><dd>[[header-ref author]]</dd></dl></span></header>
  [[body]]
</article>
|#
