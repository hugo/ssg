(define-module (ssg widgets current-date)
  :use-module (datetime)
  :export (doc widget))

(define doc "Outputs the timestamp of page rendering, in a time tag.")

(define now #f)

(define* (widget #:optional (fmt "~Y-~m-~d ~H:~M"))
  "current-date

Displays the current date.

Arguments:
- fmt :: How to print the date"
  ;; TODO current-datetime gives UTC time, but local time is probably what we want here
  (unless now (set! now (current-datetime)))
  (delay
    `(time (@ (datetime ,(datetime->string now "~Y-~m-~dT~H:~M:~S")))
           ,(datetime->string now fmt))))
