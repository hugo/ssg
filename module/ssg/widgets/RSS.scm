(define-module (ssg widgets RSS)
  :use-module (ssg file-set)
  :use-module (srfi srfi-71)
  :use-module (hnh util path)
  :use-module ((ssg lib) :select (link-or-copy))
  :use-module ((ssg assets) :select (get-asset))
  :use-module ((ssg widgets link) :select (link-to))
  :export (doc widget))

;; TODO /feed must be synced with where the feed actually ends up

(define doc "
Displays either a <link rel=\"alternative\"> tag for the HTML head, or a
clickable image. The clickable image uses the asset \"feed-icon-28x28.png\",
which should be an appropriately sized RSS logo.
")

(define* (widget variant #:key (title "RSS-flöde"))
  "RSS

Emits a clickable image, linking to the RSS feed for the site."
  (file-set-add! (file-set) "feed-icon-28x28.png"
                 (delay
                   (lambda (destination)
                     (link-or-copy (let ((f port (get-asset "feed-icon-28x28.png")))
                                     (close-port port)
                                     f)
                                   (path-append destination "feed-icon-28x28.png"))))
                 #:cookie "733f8343-66e7-41f0-a9f8-c455a1e2ee79")
  (delay
    (begin
     (unless (member "feed.rss" (file-set-keys (file-set)))
       (format (current-error-port) "RSS linked but /feed.rss doesn't exist~%"))

     (if (not (string? variant))
         '(div "RSS-widget must have a string argument")
         (case (string->symbol variant)
           ((metalink) `(link (@ (rel "alternative")
                                 (type "application/rss+xml")
                                 (title ,title)
                                 (href ,(link-to "/feed.rss")))))
           ((badge)
            `(span (@ (class "rss-badge"))
                   (a (@ (href ,(link-to "/feed.rss")))
                      (img (@ (src ,(link-to "/feed-icon-28x28.png"))
                              (alt ,title)))))))))))
