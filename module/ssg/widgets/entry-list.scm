(define-module (ssg widgets entry-list)
  :use-module (ssg entries)
  :export (doc widget))

(define doc "
A list of links to all entries.

Ignores all its parameters.
")

(define (widget . _)
  "entry-list

Emits a (hyperlinked) list of all entries, suitable for inclusion
in an HTML document."
  (delay
   `(ul ,@(map (lambda (header)
                 `(li (a (@ (href ,(entry-slug header)))
                         ,(entry-header header 'title))))
               (entries-in-reverse-chronological-order)))))
