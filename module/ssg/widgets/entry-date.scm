(define-module (ssg widgets entry-date)
  :export (entry-date widget))

(define entry-date (make-parameter "YYYY-MM-DD"))

(define (widget . _)
  "entry-date

Emits the date the entry was created."
  ;; TODO could we call the `time` widget here?
  (delay `(time (@ (datetime ,(entry-date)))
                ,(entry-date))))
