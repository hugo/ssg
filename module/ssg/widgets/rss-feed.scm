(define-module (ssg widgets rss-feed)
  :use-module (datetime)
  :use-module ((hnh util) :select (begin1 ->))
  :use-module ((ssg content-type) :select (parser-for))
  :use-module (srfi srfi-1)
  :use-module (ssg entries)
  :use-module (ssg widgets)
  :use-module (ssg templates)
  :use-module (ssg config)
  :use-module ((ssg widgets link) :select (full-link-to))
  :export (widget)
  )

(define (logb b n)
  (/ (log n) (log b)))

(define (log2 n)
  (logb 2 n))

;; returns a list of octets, containing the n utf-8 coded
(define (utf-8-encode n)
  (if (< n 128)
      (list n)
      (let* ((trailing-count
              (-> n log2
                  (floor-quotient 6)
                  inexact->exact)))
        (cons
         (logior
          (ash (1- (expt 2 (1+ trailing-count))) (- 8 (1+ trailing-count)))
          (ash n (-> (log2 n) (floor-quotient 2) inexact->exact (* 2) -)))

         (map (lambda (i)
                (logior
                 #x80
                 (logand (1- (expt 2 6))
                         (ash n (- (* i 6))))))
              (reverse (iota trailing-count)))))))

(define char-set:uri
  (char-set-union (char-set #\/)
                  (char-set-intersection char-set:ascii
                                         char-set:letter+digit)))

;; A quick and dirty url encode, to appease the RSS validator (www.rssboard.org/rss-validator)
(define (url-encode str)
  (with-output-to-string
    (lambda ()
     (string-for-each
      (lambda (c)
        (if (char-set-contains? char-set:uri c)
            (display c)
            (for-each
             (lambda (n) (format #t "%~2'0x" n))
             (utf-8-encode (char->integer c)))))
      str))))

(define (CDATA . body)
  `(,(lambda () (display "<![CDATA["))
    ,@body
    ,(lambda () (display "]]>"))))

(define (rfc-822 dt)
  (let ((locale (setlocale LC_TIME)))
    (setlocale LC_TIME "en_US.UTF-8")
    (begin1
     ;; TODO Allow setting timezone
     ;; NOTE RSS Apparentlry requires GMT instead of UTC, per RFC 822
     (datetime->string dt "~a, ~d ~b ~Y ~H:~M:~S GMT")
     (setlocale LC_TIME locale))))

(define (format-entry entry)

  (define permalink (full-link-to (url-encode (entry-slug entry))))
  (define pub-date (rfc-822 (string->datetime (entry-header entry 'date))))

  ;; TODO rss prefers to have links in the bodies of entries be absolute
  ;; (https://example.com/page) rather than just (/page)
  `(item (title ,(entry-header entry 'title))
         (link ,permalink)
         (guid ,permalink)
         (description
          ,(CDATA
            (let ((body-ast ((parser-for (or (entry-header entry 'content-type)
                                             "text/html"))
                             (entry-body entry))))
              (force-widgets (expand-templates body-ast))
             )))
         (pubDate ,pub-date)
         ;; author
         ;; category
         ))

;; https://validator.w3.org/feed/docs/rss2.html
(define (widget . _)
  "rss-feed

Emits a complete RSS feed for all entries in the page."
  (define items (map format-entry (entries-in-reverse-chronological-order)))
  (define build-date (rfc-822 (current-datetime)))
  (define link (string-append ((@ (ssg config) current-scheme))
                              "://"
                              (assoc-ref (config) 'host)
                              (or (assoc-ref (config) 'url-base) "")))
  (delay
   `(*TOP*
     (*PI* xml "version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"")
     ;; (*PI* xml-stylesheet "href=\"/static/rss.xsl\" type=\"text/xsl\"")
     (rss (@ (version "2.0"))
          ;; TODO required fields in config, and validate that
          (channel (title ,(assoc-ref (config) 'title))
                   (link ,link)
                   ;; TODO atom:link rel="self"
                   (description
                    ,(or (assoc-ref (config) 'rss-description)
                         "Please tell the feed owner to set a description. --SSG"))
                   ;; (language "sv-se")
                   (lastBuildDate ,build-date)
                   ;; (docs "")
                   (generator ,(format #f "SSG/~a" (@ (ssg version) version)))
                   ;; (managingEditor)
                   ;; (webMaster "")
                   ,@(cond ((assoc-ref (config) 'rss-image-url)
                            => (lambda (img)
                                 `((image (url ,img)
                                          (title ,(assoc-ref (config) 'title))
                                          (link ,link)))))
                           (else '()))
                   ,@items))))
  )
