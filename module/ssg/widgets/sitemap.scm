(define-module (ssg widgets sitemap)
  :use-module (ssg file-set)
  :use-module (sxml simple)
  :use-module (hnh util path)
  :use-module ((ssg widgets link) :select (link-to))
  :use-module (ssg config)
  :use-module ((ssg lib) :select (url-append))
  :export (generate-site-map! widget))

;; https://www.sitemaps.org/protocol.html

(define* (url-entry loc #:optional lastmod changefreq priority)
  `(s:url (s:loc ,loc)))


(define (widget . _)
  "sitemap

Emits a complete sitemap file for the site."
  ;; TODO should widgets really call file-set-add!?
  (file-set-add! (file-set) "sitemap.xsl"
                 (delay
                  (lambda (destination)
                    (with-output-to-file (path-append destination "sitemap.xsl")
                      (lambda () (sxml->xml sitemap-xsl)))))
                 #:cookie "b08e7660-f02c-4b90-b5cd-00aabd017dae")
  (delay
    `(*TOP* (*PI* xml "version=\"1.0\" encoding=\"UTF-8\"")
            (*PI* xml-stylesheet "href=\"sitemap.xsl\" type=\"text/xsl\"")
            (s:urlset (@ (xmlns:s "http://www.sitemaps.org/schemas/sitemap/0.9"))
                      ,@(map url-entry (map (lambda (s)
                                              (string-append
                                               ((@ (ssg config) current-scheme))
                                               "://"
                                               (url-append (assoc-ref (config) 'host)
                                                           (link-to s))))
                                            (sort (file-set-keys (file-set))
                                                  string<=?)))))))


(define sitemap-xsl
  `(*TOP* (*PI* xml "version=\"1.0\" encoding=\"UTF-8\"")
          (xsl:stylesheet (@ (version "1.0")
                             (xmlns:xsl "http://www.w3.org/1999/XSL/Transform")
                             (xmlns "http://www.w3.org/1999/xhtml"))
                          (xsl:output (@ (method "html")
                                         (indent "yes")
                                         (encoding "UTF-8")))
                          (xsl:template
                           (@ (match "/"))
                           (html (head (title "Sitemap"))
                                 (body (h1 "Sitemap")
                                       (ul
                                        (xsl:for-each
                                         ;; TODO this should be s:urlset/s:url
                                         (@ (select "*/*"))
                                         ;; TODO these * should be s:loc
                                         (li (a (xsl:attribute (@ (name "href"))
                                                               (xsl:value-of (@ (select "*"))))
                                                (xsl:value-of (@ (select "*")))))))))))))
