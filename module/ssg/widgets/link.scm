(define-module (ssg widgets link)
  :use-module (ice-9 regex)
  :use-module (srfi srfi-88)
  :use-module (ssg config)
  :use-module ((ssg lib) :select (url-append))
  :export (link-to full-link-to doc widget))

(define doc "
Given a link to a resource inside the current page, return the corresponding
(web) path for the output.

@example
<a href=\"[[link feed.rss]]\">Our RSS feed</a>
@end example

Which might produce
@example
<a href=\"/a-subsite/feed.rss\">Our RSS feed</a>
@end example
"
  )

(define (link-to item)
  "Return link to output @var{item}, with correct prefix applied."
  (url-append "/" (or (assoc-ref (config) 'url-base) "/")
              item))

(define (full-link-to item)
  (string-append ((@ (ssg config) current-scheme))
                 "://"
                 (url-append
                  (assoc-ref (config) 'host)
                  (link-to item))))

(define* (widget key: to text)
  "link

Emits the absolute URL of another page within the site."
  (delay
    `(a (@ (href ,(link-to to)))
        ,text)))
