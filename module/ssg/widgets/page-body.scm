(define-module (ssg widgets page-body)
  :export (page-body widget))

(define page-body (make-parameter "??INVALID_BODY??"))

(define (widget)
  "page-body

Emits the contents of the current page.
Should only be used by templates."
  (delay (page-body)))
