(define-module (ssg widgets mailto)
  :export (doc widget))

(define doc "
Generates HTML anchors (@code{a}-tags) with mailto url's.

@subsection Parameters
@table @samp
@item address
Mail recipient. Will be used for both the hyperlink, and the text portion.
@end table
")

(define (widget address)
  "mailto

Generates a hyperlink with a mailto: href"
  (delay
    `(a (@ (href "mailto:" ,address)) ,address)))
