(define-module (ssg widgets breadcrumbs)
  :use-module (srfi srfi-1)
  :use-module (ice-9 regex)
  :use-module ((hnh util) :select (intersperse))
  :use-module (hnh util path)
  :use-module (ssg current-page)
  :use-module ((ssg lib) :select (accumulate))
  :use-module ((ssg widgets link) :select (link-to))
  :export (breadcrumbs widget))

(define (generate-context lst)
  (reverse
   (map reverse
        (accumulate cons #f
                    (cons (list (car lst))
                          (cdr lst))))))


;; TODO final component shouldn't be a link (since we are already there)
(define (breadcrumb-for path)
  (map
   (lambda (parts)
     (let ((final-component (last parts)))
       `(a (@ (href
               ;; NOT path-join, since this is a web path
               ,(link-to (string-join parts "/"))))
           ,(string-titlecase
             (cond ((string-match "^(.*).html$" final-component)
                    => (lambda (m) (match:substring m 1)))
                   (else final-component))))))
   (remove
    (lambda (parts)
      (string=? "index.html" (last parts)))
    (generate-context (path-split path)))))


(define doc "
A nav link containing <a> tags for this and each parent page, along with some
delimiters.

It recommends the following CSS
@example
.breadcrumbs > * {
	margin: 0.5ch;
	text-decoration: none;
}

.breadcrumbs a {
	color: blue;
}
@end example
")


(define (widget)
  "breadcrumbs

Display a hyperrefed list of the path to the current page"
  (delay
    `(nav (@ (class "breadcrumbs"))
          ,@(intersperse
             '(span "»")
             (cons `(a (@ (href ,(link-to "/"))) "⌂")
                   (breadcrumb-for (current-page)))))))
