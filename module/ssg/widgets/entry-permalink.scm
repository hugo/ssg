(define-module (ssg widgets entry-permalink)
  :export (entry-permalink widget))

(define entry-permalink (make-parameter "/no-permalink-set/"))

(define (widget . args)
  "entry-permalink

Emits the permalink for the current entry."
  (delay
    (if (null? args)
        (entry-permalink)
        `(a (@ (href ,(entry-permalink)))
            ,(string-join args " ")))))
