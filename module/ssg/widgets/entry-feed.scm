(define-module (ssg widgets entry-feed)
  :use-module (datetime)
  :use-module ((ssg content-type) :select (parser-for))
  :use-module (srfi srfi-1)
  :use-module (ssg entries)
  :use-module (ssg widgets)
  :use-module (ssg templates)
  :use-module ((ssg widgets link) :select (link-to))
  :export (doc widget))


(define (format-entry entry)
  ;; TODO make this format be configurable
  `(article (h3 (time ,(entry-header entry 'date)) " — " ,(entry-header entry 'title))
            ,(let ((body-ast ((parser-for (or (entry-header entry 'content-type)
                                              "text/html"))
                              (entry-body entry))))
               (force-widgets (expand-templates body-ast)))
            (footer
             (a (@ (href ,(link-to (entry-slug entry))))
                "Permanent länk till det här inlägget"))))


(define doc "
A list of news entries, each in a @code{<details/>} tag, for both easy overview and reading.

@subsection Keyword parameters
@table @samp
@item count
How many entries to include, going from newest to oldest.
@end table
")


(define* (widget #:key count)
  "entry-feed

Emits a feed of entries, suitable for inclusion in an HTML page.
"
  (let ((formatted-entries
         (map (lambda (entry)
                ;; TODO make this format be configurable
                `(details (summary (time ,(entry-header entry 'date)) " — " ,(entry-header entry 'title))
                          ,(format-entry entry)))
              (let ((entries (entries-in-reverse-chronological-order)))
                (if count
                    (take entries (min count (length entries)))
                    entries)))))
    (delay formatted-entries)))
