(define-module (ssg widgets generator-tag)
  :export (doc widget))

(define doc
  "Outputs @code{<meta name=\"generator\" content=\"SSG @emph{version}\">}")

(define (widget variant)
  "generator-tag

Emits information about the current SSG version."
  (delay
    ;; TODO this error isn't used, since "wrong number of arguments"
    ;; are thrown higher up
    (if (not (string? variant))
        (error "generator-tag must have a string argument")
        (case (string->symbol variant)
          ((meta)
           `(meta
             (@ (name "generator")
                (content
                 ,(format #f "SSG v~a" (@ (ssg version) version))))))
          ((link)
           `(span
             (@ (class "generator-tag"))
             (a (@ (href "https://git.lysator.liu.se/hugo/ssg.git"))
                ,(format #f "SSG / ~a" (@ (ssg version) version)))))))))
