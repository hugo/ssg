;;; TODO either replace with with a general [[header-ref <key>]] widget,
;; or extend it to also include the site identity
;; (e.g. "<Site Identity> | <Specific Page>")

(define-module (ssg widgets page-title)
  :export (page-title widget))

(define page-title (make-parameter "??INVALID_TITLE??"))

(define (widget)
  "page-title

Emits the title of the current page.
Should only be used by templates."
  (delay (page-title)))
