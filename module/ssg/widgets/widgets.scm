(define-module (ssg widgets widgets)
  :use-module (srfi srfi-1)
  :use-module (ice-9 ftw)
  :use-module (hnh util path)
  :use-module (texinfo)
  :use-module (texinfo html)
  :use-module (ssg file-set)
  :use-module ((ssg lib) :select (link-or-copy))
  :export (doc widget))

(define doc "
Output all known widgets. Including their documentation, if available.
")

;; TODO move this to (hnh util path)
(define (path-relative-to base* path*)
  (define base (realpath base*))
  (define path (realpath path*))
  (let ((b (path-split base))
        (p (path-split path)))
    (path-join (drop p (length b)))))

(define module-path (make-object-property))

(define (find-widget-modules load-path)
  (append-map!
   (lambda (path-base)
     (define widget-base (path-append path-base "widgets"))
     (cond ((scandir widget-base)
            => (lambda (paths)
                 (filter-map
                  (lambda (path)
                    (catch 'not-widget
                      (lambda ()
                        (if (file-hidden? path)
                            (scm-error 'not-widget "find-widget-modules"
                                       "File is hidden: ~s" (list path) (list path))
                            (let ((path (path-append widget-base path)))
                              (case (stat:type (stat path))
                                ((regular)
                                 (let ((module
                                         (resolve-interface
                                          (map string->symbol
                                               (path-split
                                                (string-drop-right (path-relative-to path-base path) 4))))))
                                   (set! (module-path module) path)
                                   module))
                                ((directory) ;; TODO recurse
                                 (scm-error 'misc-error "find-widget-modules"
                                            "WARNING: Widgets in sub-directories not yet supported~%"
                                            '() #f))
                                (else (scm-error 'not-widget "find-widget-modules"
                                                 "WARNING: Weird file in load path: ~s~%"
                                                 (list path) (list path)))))))
                      (lambda _ #f)))
                  paths)))
           (else '())))
   load-path))

(define (widget-module->stexi module)
  (let ((fragment
         (texi-fragment->stexi
          (string-append (format #f "@section ~a~%" (string-join (map symbol->string (cdr (module-name module))) " "))
                         ;; TODO make source emission configurable
                         (let ((src-dest (path-append "widget-src" (basename (module-path module)))))
                           (file-set-add! (file-set) src-dest
                                          (delay
                                            (lambda (destination)
                                              (link-or-copy
                                               (module-path module)
                                               (path-append destination src-dest)))))
                           ;; TODO widget for absolute path in resulting tree
                           (format #f "@url{/~a,@code{~a}}~%~%"
                                   src-dest (module-path module)))
                         (catch 'misc-error
                           (lambda () (module-ref module 'doc))
                           (lambda _ "@emph{Which isn't documented}"))))))
    (stexi->shtml fragment)))

(define (widget . _)
  "widgets

Emit a list of all known widgets."
  (delay
    (let ((widget-modules (find-widget-modules %load-path)))
      `(div (@ (class "widget-widget"))
            ,@(map widget-module->stexi widget-modules)))))
