(define-module (ssg widgets comment)
  :export (doc widget))

(define doc "A widget which explicitly outputs nothing. Ignores all its arguments")

(define (widget . _)
  "comment

Does nothing. Usefull for entering comments not visible in the
rendered document."
  (delay ""))
