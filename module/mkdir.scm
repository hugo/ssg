(define-module (mkdir)
  :use-module (hnh util path)
  :use-module ((srfi srfi-1) :select (remove))
  :use-module ((ssg lib) :select (accumulate flip))
  :export (mkdir-p))


(define (mkdir-trace path)
  (remove string-null?
          (reverse
           (accumulate (flip path-append)
                       "/" (path-split path)))))

(define (mkdir-p path)
  (for-each (lambda (subpath)
              (unless (file-exists? subpath)
                (mkdir subpath)))
            (mkdir-trace path)))

