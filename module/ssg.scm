(define-module (ssg)
  :use-module (srfi srfi-1)
  :use-module (ice-9 ftw)
  :use-module (ice-9 format)

  :use-module ((hnh util env) :select (with-working-directory))
  :use-module (hnh util path)

  :use-module (ssg file-set)
  :use-module ((ssg content-type) :select (parser-for))
  :use-module (ssg lib)
  :use-module (ssg widgets)
  :use-module (ssg entries)
  :use-module (ssg templates)
  :use-module ((ssg widgets page-title) :select (page-title))
  :use-module ((ssg widgets page-body) :select (page-body))
  :use-module (sxml simple)
  :use-module (ssg config)
  :export (generate-site)
  )


(define (generate-page fs filename)
  (case-lambda
    ((body)
     (display (format #f "Page ~a lacks headers~%" filename) (current-error-port))
     ((generate-page fs filename) '() body))

    ((headers body)
     (let ((template-name (or (assoc-ref headers 'template)
                              (assoc-ref (config) 'default-template)
                              (error "Template Required")))
           (target-file (path-join (cdr (path-split filename)))))

       ;; Target file is the subpath under pages in the input directory
       (file-set-add!
        fs target-file
        (delay
          (let ((body-ast ((parser-for (or (assoc-ref headers 'content-type)
                                           "text/html"))
                           body))
                (template-ast (read-template template-name))
                (page-title*
                 (or (assoc-ref headers 'title)
                     (assoc-ref (config) 'title)
                     (assoc-ref default-config 'title))))
            (lambda (destination)
              ;; runs widget setup code
              (define template-resolved (expand-templates template-ast))
              (define body-resolved (expand-templates body-ast))
              ;; forces widgets
              (define final-tree
                (parameterize ((page-body (force-widgets body-resolved))
                               (page-title page-title*))
                  (force-widgets template-resolved)))
              (with-output-to-file (path-append destination target-file)
                (lambda () (sxml->xml final-tree)))))))))))


(define (load-static-pages! fs)
  (nftw "static"
        (lambda (path stat flag base level)
          (let ((dest-components (cdr (path-split path))))
            ;; Dest components are null for the root (which already must be a directory).
            ;; path-join doesn't handle zero components.
            (unless (null? dest-components)
              (let ((dest-path (path-join dest-components)))
                (case flag
                  ;; Simlinks contents are copied verbatim
                  ((symlink)
                   (let ((source (realpath path)))
                     (file-set-add! fs dest-path
                                    (delay
                                      (lambda (destination)
                                        (symlink-if-not-exists
                                         (readlink source)
                                         (path-append destination dest-path)))))))
                  ((regular) ; This includes sockets, pipes, ...
                   (let ((source-file (realpath path)))
                     (file-set-add! fs dest-path
                                    (delay
                                      (lambda (destination)
                                        (link-or-copy source-file
                                                      (path-append destination dest-path)))))))
                  ((directory) 'noop)
                  (else ; This includes a number of weird error cases
                   (display (format #f "~a in static/~a, ignoring~%"
                                    flag dest-path)
                            (current-error-port)))))))
          #t)
        'physical)  )


;;; == Add fully dynamically generated pages ==
(define (load-fully-dynamic-pages! fs)
  ;; Misc error is thrown on both missing module, and on missing value in module.
  (catch 'misc-error
    (lambda ()
      (for-each (lambda (pair)
                  (file-set-add!
                   fs (car pair)
                   (delay
                     (lambda (destination)
                       (with-output-to-file (path-append destination (car pair))
                         (lambda () (sxml->xml (force (cdr pair)))))))))
                (@ (pages) pages)))
    (lambda (err proc fmt fmt-args data)
      (display (format #f "~a in ~a: ~?~%" err proc fmt fmt-args)
               (current-error-port)))))


;;; == Add dynamicly generated pages based on pages files ==
(define (load-dynamic-pages! fs)
  (ftw "pages"
       (lambda (filename statinfo flag)
         (case flag
           ((regular)
            ;; parameterize ((breadcrumbs (breadcrumb-from (dirname filename))))
            (let ((sourcefile (realpath filename)))
              (call-with-values (lambda () (read-entry sourcefile))
                (generate-page fs filename))))

                ((directory) 'directories-are-noop)

                (else (display (format #f "Unhandled file (~a): ~s~%" flag filename)
                               (current-error-port))))
              #t)))

(define (generate-site site-dir output-dir)
  (with-working-directory
   site-dir
   (lambda ()
     (define fs (file-set))
     (config (call-with-input-file "config" read-key-value))

     (let ((assumed-version (assoc-ref (config) 'ssg-version)))
       (cond ((not assumed-version)
              (format (current-error-port)
                      "  Target ssg version not specified in config, expected output can't be guaranteed~%  (add `ssg-version: ~a' to your configuration file to suppress)~%"
                      (@ (ssg version) version)))
             ((not (equal? assumed-version (@ (ssg version) version)))
              (format (current-error-port)
                      "  Target SSG version and actual ssg version doesn't match~%  SSG version: ~a~%  Target version of site: ~a~%  Expected output can't be guaranteed~%"
                      (@ (ssg version) version)
                      assumed-version))
             (else
              ;; Versions match, all fine
              'noop)))

     (cond ((assoc-ref (config) 'slug-chars)
            => (lambda (cs) ((@ (ssg entries) slug-chars)
                        (primitive-eval (call-with-input-string cs read))))))

     (add-to-load-path (getcwd))

     (load-entries! fs (config))

     (load-static-pages! fs)

     (parameterize ((file-set fs))
       (load-fully-dynamic-pages! fs)
       (load-dynamic-pages! fs)

       (realize-file-set fs output-dir))
       )))
