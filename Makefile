.PHONY: all set-version

all: ssg.info

set-version:
	$(if $(value VERSION),,$(error "VERSION not set"))
	sed -i "s/\\(#|@version@|#\\) \"[^\"]*\"/\\1 \"$(VERSION)\"/" module/ssg/version.scm
