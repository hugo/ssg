SSG - A Static Site Generator
=============================

Documentation is [currently available as TexInfo](ssg.texi).

License and copying
-------------------

Copyright Hugo Hörnquist 2023

The software is distributed under [AGPL (3.0 or later)](LICENSE).
